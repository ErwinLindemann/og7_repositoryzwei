package controller;

import java.util.LinkedList;
import java.util.List;

import model.Rechteck;
import model.MySQLDatabase;
import view.Erstellen;

public class BunteRechteckeController {

	public static void main(String[] args) {
		
	}

	private List<Rechteck> rechtecke;
	private MySQLDatabase database;
	

	public BunteRechteckeController() {
		super();
		//this.rechtecke = new LinkedList();
		this.database = new MySQLDatabase();
		rechtecke = this.database.getAlleRechtecke();
	}

	public void add(Rechteck rechteck) {
		rechtecke.add(rechteck);
		this.database.rechtecke_eintragen(rechteck);
	}
	
	public void reset() {
		rechtecke.clear();
	}

	public List<Rechteck> getRechtecke() {
		return rechtecke;
	}

	public void setRechtecke(List rechtecke) {
		this.rechtecke = rechtecke;
	}
	
	@Override
	public String toString() {
		return "BunteRechteckeController [rechtecke=" + rechtecke + "]";
	}
	
	public void generiereZufallsRechtecke(int anzahl) {
		this.reset();
		for (int i = 0; i < anzahl ; i++) {
			rechtecke.add(Rechteck.generiereZufallsRechteck());
		}
	}

	public void rechteckHinufuegen() {
		new Erstellen(this).setVisible(true);;
		
	}
	
}