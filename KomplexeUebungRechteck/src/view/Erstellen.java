package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import controller.BunteRechteckeController;
import model.Rechteck;

public class Erstellen extends JFrame {

	private JPanel contentPane;
	private final JLabel lblNewLabel_3 = new JLabel("Breite");
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private final JTextField textField_3 = new JTextField();
	private BunteRechteckeController brc = new BunteRechteckeController();

	/**
	 * Launch the application.
	 */
	
	/**
	 * Create the frame.
	 */
	public Erstellen(BunteRechteckeController brc) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(9, 0, 0, 0));
		
		JLabel lblNewLabel_1 = new JLabel("X");
		contentPane.add(lblNewLabel_1);
		
		textField = new JTextField();
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Y");
		contentPane.add(lblNewLabel_2);
		
		textField_1 = new JTextField();
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		contentPane.add(lblNewLabel_3);
		
		textField_2 = new JTextField();
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("H\u00F6he");
		contentPane.add(lblNewLabel);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		JButton btnNewButton = new JButton("Erstellen");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Rechteck erstelle = new Rechteck(Integer.parseInt(textField.getText()),Integer.parseInt(textField_1.getText()),Integer.parseInt(textField_2.getText()),Integer.parseInt(textField_3.getText()));
				brc.add(erstelle);
				textField.setText("");
				textField_1.setText("");
				textField_2.setText("");
				textField_3.setText("");
			}
		});
		contentPane.add(btnNewButton);
	}

}
