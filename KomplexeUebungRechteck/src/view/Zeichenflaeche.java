package view;

import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Graphics;

import controller.*;
import model.*;

public class Zeichenflaeche extends JPanel {

	private BunteRechteckeController controllerobjekt2;

	public Zeichenflaeche(BunteRechteckeController controllerobjekt2) {
		this.controllerobjekt2 = controllerobjekt2;
	}

	@Override
	public void paintComponent(Graphics g) {
		g.setColor(Color.BLACK);
		for (Rechteck rechteck : controllerobjekt2.getRechtecke()) {
			g.drawRect(rechteck.getX(), rechteck.getY(), rechteck.getBreite(), rechteck.getHoehe());
		}

	}
}