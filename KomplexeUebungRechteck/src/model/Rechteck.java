package model;

import java.util.Random;

public class Rechteck {

	int x;
	int y;
	int breite;
	int hoehe;
	Punkt p;
	
	public Rechteck(int x, int y, int breite, int hoehe) {
		super();
		this.x = x;
		this.y = y;
		this.breite = breite;
		this.hoehe = hoehe;
	}

	public Rechteck() {
		super();
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getBreite() {
		return breite;
	}

	public void setBreite(int breite) {
		breite = Math.abs(breite);
		this.breite = breite;
	}

	public int getHoehe() {
		return hoehe;
	}

	public void setHoehe(int hoehe) {
		hoehe = Math.abs(hoehe);
		this.hoehe = hoehe;
	}

	@Override
	public String toString() {
		return "Rechteck [x=" + x + ", y=" + y + ", breite=" + breite + ", hoehe=" + hoehe + "]";
	}
	
	public boolean enthaelt(int x, int y) {
		if (this.x <= x && this.x + this.getBreite() >= x && this.y <= y && this.y + this.getHoehe() >= y) {
			return true;
		}
		return false;
	}
	
	public boolean enthaelt(Punkt p) {
		return this.enthaelt(p.getX(), p.getY());
	}

	public boolean enthaelt(Rechteck rechteck) {
		Punkt obenlinks = new Punkt(rechteck.getX(), rechteck.getY());
		Punkt untenrechts = new Punkt(rechteck.getX() + rechteck.getBreite(), rechteck.getY() + rechteck.getHoehe());
		if (enthaelt(obenlinks) && enthaelt(untenrechts)) {
			return true;
		}
		return false;
	}

	public static Rechteck generiereZufallsRechteck() {
		Random random = new Random();
		int x = random.nextInt(1200);
		int y = random.nextInt(1000);
		int breite = random.nextInt(1200-x)+1;
		int hoehe = random.nextInt(1000-y)+1;
		Rechteck rechteck = new Rechteck(x, y, breite, hoehe);
		return rechteck;
	}
	
}
