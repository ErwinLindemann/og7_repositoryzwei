package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class MySQLDatabase {
	private final String driver = "com.mysql.jdbc.Driver";
	private final String url = "jdbc:mysql://localhost/rechtecke?";
	private final String user = "root";
	private final String password = "";
	
	public void rechtecke_eintragen(Rechteck r) {
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			
			String sql = "INSERT INTO T_Rechtecke (x,y,breite,hoehe) VALUES (?,?,?,?);";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, r.getX());
			ps.setInt(2, r.getY());
			ps.setInt(3, r.getBreite());
			ps.setInt(4, r.getHoehe());
			ps.executeUpdate();
			
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<Rechteck> getAlleRechtecke(){
		List rechtecke = new LinkedList<Rechteck>();
		try {
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "SELECT * FROM T_Rechtecke;";
			Statement s = con.createStatement();
			ResultSet rs = s.executeQuery(sql);
			while(rs.next()) {
				int x = rs.getInt("x");
				int y = rs.getInt("Y");
				int breite = rs.getInt("Breite");
				int hoehe = rs.getInt("Hoehe");
				rechtecke.add(new Rechteck(x,y,breite,hoehe));
			}
			
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rechtecke;
	}
	
}
